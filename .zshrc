# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Helper function - return true if the command 1st arg is installed.
# Suppreses all output.
function exists_cmd()
{
  command -v $1 > /dev/null 2>&1
}

# Helper function - return true if the directory given by the 1st arg exists.
function exists_dir()
{
  [[ -d $1 ]]
}

# Helper function - return true if the file given by the 1st arg exists.
function exists_file()
{
  [[ -a $1 ]]
}

# Helper function - sources the file given by the 1st arg only if it exists.
# Produces no output.
function source_if_exists()
{
  if exists_file $1 ; then
	  source $1
	fi
}

source_if_exists $HOME/.nix-profile/etc/profile.d/nix.sh

# Use ccache.
export PATH="/usr/lib/ccache/:$PATH"

# XDG_Base_Directory
# User directories
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
#export XDG_DATA_HOME="$HOME/.local/share"
## System directories
#export XDG_DATA_DIRS="/usr/local/share:/usr/share"
#export XDG_CONFIG_DIRS="/etc/xdg"

# Check if zplug is installed and install it if not
export ZPLUG_HOME="$XDG_CONFIG_HOME/zsh/zplug"
if ! exists_dir $ZPLUG_HOME ; then
  git clone https://github.com/zplug/zplug $ZPLUG_HOME
  source $ZPLUG_HOME/init.zsh && zplug update --self
fi

# zplug
source "$ZPLUG_HOME/init.zsh"

# Use 256 colors
zplug "chrissicool/zsh-256color"

# Suggestions as you type
zplug "zsh-users/zsh-autosuggestions"

# Syntax highlighting
zplug "zdharma/fast-syntax-highlighting", defer:1

# Better reverse history search MUST BE LOADED AFTER SYNTAX HIGHLIGHTING!
zplug "zsh-users/zsh-history-substring-search", defer:2

# Completions not yet in Zsh
zplug "zsh-users/zsh-completions", lazy:true

# Only for Ubuntu and openSUSE: If a command is not recognized in the $PATH,
# this will use Ubuntu's command-not-found package to find it or suggest
# spelling mistakes
zplug "plugins/command-not-found", from:oh-my-zsh, lazy:true

# Press ESC twice to add sudo to command
zplug "plugins/sudo", from:oh-my-zsh, lazy:true

# Auto start SSH agent
zplug "plugins/ssh-agent", from:oh-my-zsh

# Auto connect to Tmux if it's running and aliases
zplug "plugins/tmux", from:oh-my-zsh, lazy:true
# Auto connect to tmux
export ZSH_TMUX_AUTOSTART=false

zplug "plugins/colored-man-pages", from:oh-my-zsh, lazy:true

# Sets lots of options related to the history file
# https://github.com/sorin-ionescu/prezto/blob/master/modules/history/init.zsh
zplug "modules/history", from:prezto

# Set terminal window and tabs names automatically to be current dir or command.
zplug "modules/terminal", from:prezto, lazy:true

# Allows vim file:123 to open file at line 123
zplug "nviennot/zsh-vim-plugin", lazy:true

# Provides 'gi' command for creating git ignore files using gitignore.io
#zplug "voronkovich/gitignore.plugin.zsh"

zplug "laggardkernel/git-ignore"
alias gi="git-ignore"

# Zplug will update itself with other packages
zplug 'zplug/zplug', hook-build:'zplug --self-manage', lazy:true

# Theme
#zplug "subnixr/minimal"
zplug "mafredri/zsh-async", from:github
zplug "sindresorhus/pure", use:pure.zsh, from:github, as:theme

# Install packages that have not been installed yet
if ! zplug check --verbose; then
    zplug install
fi

# Then, source plugins and add commands to $PATH
zplug load

# FZF
source_if_exists $HOME/.nix-profile/share/fzf/key-bindings.zsh
source_if_exists $HOME/.nix-profile/share/fzf/completion.zsh

# Change history file to be zsh_history. Otherwise modules/history will set it
# to zhistory.
HISTFILE="$XDG_CONFIG_HOME/zsh/history"

# Stop .lesshst file being created in $HOME
export LESSKEY="$XDG_CONFIG_HOME"/less/lesskey
export LESSHISTFILE="$XDG_CACHE_HOME"/less/history

# Move ccache out of $HOME
export CCACHE_CONFIGPATH="$XDG_CONFIG_HOME"/ccache.config
export CCACHE_DIR="$XDG_CACHE_HOME"/ccache

# Fullscreens the current window and works from within tmux
alias fullscreen='wmctrl -ir $(xdotool getwindowfocus) -b toggle,fullscreen'

if exists_cmd nvim ; then
  alias vim=nvim
fi

if exists_cmd exa; then
  alias ls=exa
fi

if exists_cmd bat; then
  alias cat=bat
fi

if exists_cmd thefuck; then
  eval $(thefuck --alias f)
fi

# This is a function not an alias to discard extra arguments
if exists_cmd tldr; then
  function h() {
    tldr $1
  }
fi

if exists_cmd xdg-open; then
  function o() {
    xdg-open $1 & ; disown
  }
fi

function mkcd() {
  mkdir $1
  cd $1
}

# Auto start ssh-agent
#eval $(ssh-agent)
# Makes SSH agent forwarding work with tmux
export SSH_AUTH_SOCK=$HOME/.ssh/ssh_auth_sock

# history-substring-search
# Binds history-substring-search to up and down keys
# Needs to be after zplug "history-substring-search line
# zplug check returns true if the given repository exists
if zplug check zsh-users/zsh-history-substring-search; then
  bindkey '^[[A' history-substring-search-up
  bindkey '^[[B' history-substring-search-down
  # Increases compatibility with more terminals
  bindkey "$terminfo[kcuu1]" history-substring-search-up
  bindkey "$terminfo[kcud1]" history-substring-search-down
fi

# fix delete key
bindkey '^[[3~' delete-char-or-list

HISTORY_SUBSTRING_SEARCH_FUZZY=1

# Anyenv
export ANYENV_ROOT="$XDG_CONFIG_HOME/anyenv"
export PATH="$XDG_CONFIG_HOME/anyenv/bin:$PATH"
if exists_cmd anyenv; then
  eval "$(anyenv init -)"
fi
